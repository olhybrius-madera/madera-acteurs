from django.apps import AppConfig


class ActeursConfig(AppConfig):
    name = 'acteurs'
