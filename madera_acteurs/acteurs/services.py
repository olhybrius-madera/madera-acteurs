import pika
from django.conf import settings
from keycloak import KeycloakAdmin


class UtilisateurService:

    def __init__(self):
        # Il faut ajouter un "/" à l'URL pour que la connexion fonctionne. On ne peut pas le mettre au niveau de la
        # variable d'environnement directement, sinon django-keycloak-auth ne fonctionne plus
        self.keycloak_admin = KeycloakAdmin(server_url=f"{settings.KEYCLOAK_CONFIG['KEYCLOAK_SERVER_URL']}/",
                                            username='admin',
                                            password='admin',
                                            realm_name=settings.KEYCLOAK_CONFIG['KEYCLOAK_REALM'],
                                            user_realm_name='master',
                                            auto_refresh_token=['get', 'put', 'post', 'delete'],
                                            verify=False)

    def creer_utilisateur(self, user_infos):
        user_uuid = self.keycloak_admin.create_user({"email": user_infos.mail,
                                                     "username": f'{user_infos.nom}{user_infos.prenom[0]}'.lower(),
                                                     "enabled": True,
                                                     "firstName": user_infos.prenom,
                                                     "lastName": user_infos.nom,
                                                     "credentials": [
                                                         {
                                                             "value": f'{user_infos.nom}{user_infos.prenom[0]}'.lower(),
                                                             "type": "password",
                                                         }
                                                     ]})
        return user_uuid

    def mettre_a_jour_utilisateur(self, user_infos):
        self.keycloak_admin.update_user(user_infos.uuid_utilisateur,
                                        {"email": user_infos.mail,
                                         "username": f'{user_infos.nom}{user_infos.prenom[0]}',
                                         "firstName": user_infos.prenom,
                                         "lastName": user_infos.nom})

    def assigner_role(self, role_name, user_uuid):
        roles = self.keycloak_admin.get_realm_roles()
        role_id = next(filter(lambda r: r['name'] == role_name, roles))['id']
        self.keycloak_admin.assign_realm_roles(user_uuid, settings.KEYCLOAK_CONFIG['KEYCLOAK_CLIENT_ID'],
                                               {"id": role_id, "name": role_name})


class MessageService:

    def __init__(self):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)
        connection = pika.BlockingConnection(parameters)
        self.channel = connection.channel()
        self.channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)

    def envoyer_message(self, routing_key, body):
        self.channel.basic_publish(exchange='madera', routing_key=routing_key, body=body)
