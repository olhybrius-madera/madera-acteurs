import json

from django.core.management.base import BaseCommand, CommandError
import pika
from django.conf import settings
from socket import gaierror
import time

from rest_framework.renderers import JSONRenderer

from acteurs.models import Client
from acteurs.services import UtilisateurService, MessageService
from acteurs.serializers import ClientSerializer


class Command(BaseCommand):
    help = 'RabbitMQ consumer example for Madera app'

    def handle(self, *args, **options):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)
        subscriptions = settings.SUBSCRIPTIONS.split(',')

        while True:
            try:
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)
                channel.queue_declare(queue=settings.BROKER_QUEUE, durable=True)

                print('sub', subscriptions)
                for subscription in subscriptions:
                    channel.queue_bind(exchange='madera', queue=settings.BROKER_QUEUE, routing_key=subscription)

                print(' [*] Waiting for messages. To exit press CTRL+C')
                channel.basic_consume(queue=settings.BROKER_QUEUE, on_message_callback=self.callback)

                channel.start_consuming()
            except (pika.exceptions.ConnectionClosed, pika.exceptions.AMQPConnectionError, gaierror):
                print('Broker unreachable, retrying to connect in', settings.BROKER_CONNECTION_RETRY_INTERVAL,
                      'seconds')
                time.sleep(settings.BROKER_CONNECTION_RETRY_INTERVAL)
                continue
            except KeyboardInterrupt:
                channel.stop_consuming()

            connection.close()
            break

    def callback(self, ch, method, properties, body):
        print(" [x] %r:%r" % (method.routing_key, body))
        payload = json.loads(body)
        utilisateur_service = UtilisateurService()
        message_service = MessageService()

        if method.routing_key == 'client_erp.cree':
            uuid = payload.pop('uuid')
            client, created = Client.objects.get_or_create(uuid=uuid, **payload)
            uuid_utilisateur = utilisateur_service.creer_utilisateur(client)
            client.uuid_utilisateur = uuid_utilisateur
            serializer = ClientSerializer(client)
            client_json = JSONRenderer().render(serializer.data)
            message_service.envoyer_message('client.cree', client_json)
            utilisateur_service.assigner_role('client', uuid_utilisateur)

        if method.routing_key == 'client_erp.maj':
            uuid = payload.pop('uuid')
            Client.objects.filter(uuid=uuid).update(**payload)
            client = Client.objects.get(uuid=uuid)
            utilisateur_service.mettre_a_jour_utilisateur(client)
            serializer = ClientSerializer(client)
            client_json = JSONRenderer().render(serializer.data)
            message_service.envoyer_message('client.maj', client_json)

        ch.basic_ack(delivery_tag=method.delivery_tag)
