from django.core.validators import RegexValidator
from django.db import models
import uuid


class CodePostalValidator(RegexValidator):
    regex = r'^[0-9]{5}$'
    message = 'Code postal invalide.'


class Client(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    adresse = models.CharField(max_length=50)
    complement_adresse = models.CharField(max_length=50, blank=True)
    code_postal = models.CharField(max_length=50, validators=[CodePostalValidator()])
    ville = models.CharField(max_length=50)
    telephone = models.CharField(max_length=50)
    mail = models.EmailField(max_length=50)
    uuid_utilisateur = models.UUIDField(null=True)
