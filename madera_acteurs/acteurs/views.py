from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .services import UtilisateurService, MessageService
from .models import Client
from .serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    keycloak_roles = {
        'GET': ['commercial'],
        'POST': ['commercial'],
        'PUT': ['commercial'],
    }

    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        utilisateur_service = UtilisateurService()
        client = serializer.save()
        uuid_utilisateur = utilisateur_service.creer_utilisateur(client)
        client.uuid_utilisateur = uuid_utilisateur
        utilisateur_service.assigner_role('client', uuid_utilisateur)
        serializer = self.get_serializer(client)
        message_service = MessageService()
        client_json = JSONRenderer().render(serializer.data)
        message_service.envoyer_message('client.cree', client_json)
        return Response(serializer.data)
